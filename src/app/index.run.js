(function() {
  'use strict';

  angular
    .module('ptAplportal')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('Aplportal: runBlock end');
  }

})();
