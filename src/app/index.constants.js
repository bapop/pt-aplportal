/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('ptAplportal')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
