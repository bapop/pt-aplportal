(function() {
  'use strict';

  angular
    .module('ptAplportal')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($location,$mdSidenav, $log) {
    var vm = this;
    
    vm.toggler = function(navID) {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      //}
    }
  }
})();
