(function() {
  'use strict';

  angular
    .module('ptAplportal')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $httpProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
    
    $httpProvider.interceptors.push(function($q) {
      return {
        'responseError': function(rejection) {
          //var msg = rejection.status+" - "+rejection.statusText + "\n"  + rejection.data;
          switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              //Authentication.user = null;
              //$injector.get('$state').transitionTo('authentication.signin');
              break;
            case 403:
              //$injector.get('$state').transitionTo('forbidden');
              break;
          }
          // Always reject (or resolve) the deferred you're given
          return $q.reject(rejection);  
        }
      };
    });
    
    
    //iE fix!    
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};    
    }    
    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    
    
  }

})();
