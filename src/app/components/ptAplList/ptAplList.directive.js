(function() {
  'use strict';

  angular
    .module('ptAplportal')
    .directive('ptAplist', ptAplist);

  /** @ngInject */
  function ptAplist() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/ptAplList/ptAplList.html',
      scope: {
        sidenavid: '@'
        //creationDate: '='
      },
      controller: AplListController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function AplListController($log, $mdToast, $attrs, AplListService, /*$timeout, */$mdSidenav, /*$mdDialog, $location,*/ $window) {
      var vm = this;

      vm.orderBy = "Title";
      vm.orderlist = [{"by":"Title", "display":"Title ASC"},{"by":"-Title", "display":"Title DESC"}];
      vm.favonly;
      vm.query="";
      vm.search = search;
      vm.openMenu = openMenu;
      vm.showSearch=false;
      vm.back = back;
      vm.refresh = activate;
      vm.favorite = favorite;
      vm.redirect = redirect;

      activate();

      function activate() {
        AplListService.get().$promise
        .then(function(data) {
          vm.userdata=data;
          vm.awesomeThings = vm.userdata.apps;
          showSimpleToast(vm.userdata.user.givenname);
        });
      }
      function showSimpleToast(name) {
        var msgs = ["Hi", "Hello", "Welcome back", "Nice to see you", "Good job"];
        var msgi = function getRandomInt(min, max) {
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        $mdToast.show(
          $mdToast
            .simple()
            .textContent(msgs[msgi(0,msgs.length-1)]+ ' ' + name)
        );
      }
      function openMenu($mdOpenMenu, ev) {
        //originatorEv = ev;
        $mdOpenMenu(ev);
      }
      function back() {
        vm.query="";
        vm.showSearch = !vm.showSearch;
      }
      function favorite(item) {
        item.IsFavorite=!item.IsFavorite;
        //need a service implementation on server-side to save data
      }
      function redirect(item) {
        if($attrs.sidenavid)
          $mdSidenav($attrs.sidenavid).close();
        if(item.RelativeURL.charAt(0)=="/") {
          $window.location.href=item.RelativeURL;
        } else {
          $window.open(item.RelativeURL, "_blank");
        }
      }
      function search(row) {
        return !!((row.Title.toLowerCase().indexOf(vm.query.toLowerCase() || '') !== -1 
        || row.Category.toLowerCase().indexOf(vm.query.toLowerCase() || '') !== -1));
      }
    }
  }

})();
