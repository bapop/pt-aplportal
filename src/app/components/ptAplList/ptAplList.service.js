(function () {
  'use strict';

  angular
    .module('ptAplportal')
    .factory('AplListService', AplListService);

  /** @ngInject */
  function AplListService($resource, $location) {
    var path = $location.absUrl().indexOf('localhost:8080') > -1 ?
      'data.json' : $location.absUrl().indexOf('localhost') > -1 ?
        'api/portal/:id' : '/popularCore/api/portal/:id';
    // return $resource('/authenticationManager/Application/List/:id', {
    return $resource(path, {
      id: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
